﻿[System.Serializable]
public class DataCard 
{
   public int id;
    public string title;
    public int price;
    public int powerConsumption;
    public int[] hashRate;
    public int hp;
    public bool isRun;
}
