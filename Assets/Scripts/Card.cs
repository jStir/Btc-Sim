﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Card : MonoBehaviour
{
    public DataCard dataCard;

   // private TextMeshProUGUI titleText;
    private ShowDataCoins dataCoins;

    private float speedMinerMOD = 1f;

    private GameObject infoPanel;
    private Button buttonCard;

    

    private void Awake()
    {
        infoPanel = GetComponentInChildren<RectTransform>().gameObject;
       // titleText = GetComponent<TextMeshProUGUI>();
        buttonCard = GetComponent<Button>();
    }


    private void Start()
    {
       // titleText.text = dataCard.title;
        buttonCard.onClick.AddListener(ShowInfoPanel);
        

    }

    public void ShowInfoPanel() 
    {
        infoPanel.SetActive(true);
    
    }

    public void StartMinerWork(int currencyType) 
    {
        StartCoroutine(StartMiner(currencyType));
    
    }

    IEnumerator StartMiner(int currencyType) 
    {
        while (dataCard.isRun)
        {
            switch (currencyType)
            {
                case 0:
                    dataCoins.BTC += dataCard.hashRate[currencyType];
                    break;
                case 1:
                    dataCoins.LTC += dataCard.hashRate[currencyType];
                    break;
                case 2:
                    dataCoins.ETH += dataCard.hashRate[currencyType];
                    break;
                case 3:
                    dataCoins.MON += dataCard.hashRate[currencyType];
                    break;

                default:
                    break;
            }
            yield return new WaitForSeconds(1f/speedMinerMOD);

        }
        
    }


}
