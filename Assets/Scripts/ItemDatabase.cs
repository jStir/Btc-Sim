﻿using LitJson;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ItemDatabase : MonoBehaviour
{
    private List<Item> database = new List<Item>();//список карт
    private JsonData itemData;

    private void Start()
    {
        itemData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Items.json"));//объект, содержащий данные карт в файле
        ConstructItemDatabase();

    }

    private void ConstructItemDatabase()
    {
        for (int i = 0; i < itemData.Count; i++)
        {
            database.Add(
                new Item(
                        (int)itemData[i]["id"],
                        itemData[i]["title"].ToString(),
                        (int)itemData[i]["price"],
                        (int)itemData[i]["powerConsumption"],
                        (int)itemData[i]["hashRate"]["btc"],
                        (int)itemData[i]["hashRate"]["ltc"],
                        (int)itemData[i]["hashRate"]["eth"],
                        (int)itemData[i]["hashRate"]["mon"],
                        (int)itemData[i]["hp"],
                        (bool)itemData[i]["isRun"]));
        }
    }


    public Item FetchItemById(int id)
    {
        for (int i = 0; i < database.Count; i++)
        {
            if (database[i].ID == id)
                return database[i];
        }
        return null;
    }
}
