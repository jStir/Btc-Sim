﻿using UnityEngine;

public class Shop : MonoBehaviour
{
    private ItemDatabase Database;
    private ShowDataCoins dataCoins;
    private UIManager uiMan;
    private Ferm ferm;

    private void Start()
    {
        uiMan = GetComponent<UIManager>();
        ferm = GetComponent<Ferm>();
        Database = GetComponent<ItemDatabase>();
        dataCoins = FindObjectOfType<ShowDataCoins>();

    }


    public void BuyItemById(int id)
    {
        if (dataCoins.Money > Database.FetchItemById(id).Price)
        {
            ferm.AddCard(new Item(
        Database.FetchItemById(id).ID,
        Database.FetchItemById(id).Title,
        Database.FetchItemById(id).Price,
        Database.FetchItemById(id).PowerConsumption,
        Database.FetchItemById(id).HashRate[0],
        Database.FetchItemById(id).HashRate[1],
        Database.FetchItemById(id).HashRate[2],
        Database.FetchItemById(id).HashRate[3]
        ));
            dataCoins.Money -= Database.FetchItemById(id).Price;
            Debug.Log("со счета снято " + Database.FetchItemById(id).Price);
            uiMan.ShowMessageOnPanel("Успех!");

        }
        else
        {
            uiMan.ShowMessageOnPanel("Недостаточно денег!");
            Debug.Log("карта " + Database.FetchItemById(id).Title + "стоит " + Database.FetchItemById(id).Price);
        }

    }

}
