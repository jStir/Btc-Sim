﻿using System.Collections;
using TMPro;
using UnityEngine;
public class UIManager : MonoBehaviour
{
    [SerializeField]
    private GameObject panelMessage;

    private TextMeshProUGUI textMesh;

    private void Start()
    {
        textMesh = panelMessage.GetComponentInChildren<TextMeshProUGUI>();
    }
    public void ShowMessageOnPanel(string text)
    {
        panelMessage.SetActive(true);
        textMesh.text = text;
        StartCoroutine(AutoHideMessagePanel());
    }

    public void HidePanel()
    {
        panelMessage.SetActive(false);
    }

    private IEnumerator AutoHideMessagePanel()
    {
        yield return new WaitForSeconds(1.5f);
        HidePanel();

    }
}
