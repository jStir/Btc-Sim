﻿using System.Collections.Generic;
using UnityEngine;

public class Ferm : MonoBehaviour
{
    [SerializeField]
    private GameObject prefabCard;
    [SerializeField]
    private RectTransform transformParentFermObj;

    public List<GameObject> Cards = new List<GameObject>();

    private void Start()
    {
       // RecountFerm();
       // Cards = LoadFerm("Cards.json");
    }


  //Тут нужно реализовать функцию загрузки фермы
    public List<GameObject> LoadFerm(string path) 
    {
      
            


        return null;
    }

    //реализовать добавление карты в список карт
    public void AddCard(Item item)//передаются данные о карте
    {
        GameObject card = Instantiate(prefabCard);//инст префаб
        card.transform.parent= transformParentFermObj;
        card.GetComponent<RectTransform>().localScale = Vector3.one;

        Card cardFunc = card.GetComponent<Card>();

        cardFunc.dataCard.id = item.ID;
        cardFunc.dataCard.title= item.Title;
        cardFunc.dataCard.price= item.Price;
        cardFunc.dataCard.powerConsumption= item.PowerConsumption;
        cardFunc.dataCard.hashRate = new int[4];
        cardFunc.dataCard.hashRate[0]= item.HashRate[0];
        cardFunc.dataCard.hashRate[1]= item.HashRate[1];
        cardFunc.dataCard.hashRate[2]= item.HashRate[2];
        cardFunc.dataCard.hashRate[3]= item.HashRate[3];
        cardFunc.dataCard.hp= item.Hp;
        cardFunc.dataCard.isRun= item.isRun;

        Cards.Add(card);

       // RecountFerm();
    }

    private void RecountFerm()
    {

        for (int i = 0; i < transformParentFermObj.childCount; i++)
        {
            Destroy(transformParentFermObj.GetChild(i));
        }




    }
}
