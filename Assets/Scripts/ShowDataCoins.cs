﻿using TMPro;
using UnityEngine;

public class ShowDataCoins : MonoBehaviour
{

    private int btc;
    private int ltc;
    private int money;
    private int eth;
    private int monero;

    public delegate void MethodCounter();
    public event MethodCounter onCount;

    public DataCoins coins = new DataCoins();

    // private string pathCoins;


    public int BTC
    {
        get { return btc; }
        set
        {
            btc = value;
            onCount?.Invoke();
        }
    }
    public int LTC
    {
        get { return ltc; }
        set
        {
            ltc = value;
            onCount?.Invoke();
        }
    }
 
    public int Money
    {
        get { return money; }
        set
        {
            money = value;
            onCount?.Invoke();
        }
    }
    public int ETH
    {
        get { return eth; }
        set
        {
            eth = value;
            onCount?.Invoke();
        }
    }
    public int MON
    {
        get { return monero; }
        set
        {
            monero = value;
            onCount?.Invoke();
        }
    }

    public TextMeshProUGUI[] textCoin;
    private string iconBTC = "<sprite index=1>";
    private string iconLTC = "<sprite index=0>";
    private string iconMoney = "<sprite index=0>";
    private string iconETH = "<sprite index=17>";
    private string iconMON = "<sprite index=12>";


    private void Start()
    {

        onCount += UpdateShowDataCoins;
        textCoin = new TextMeshProUGUI[5];
        textCoin = GetComponentsInChildren<TextMeshProUGUI>();

        LoadDataCoins();
        onCount?.Invoke();

    }

    private void LoadDataCoins()
    {

        SaveLoad.Load<DataCoins>(ref coins, "Coins.json");

        if ((coins.btc + coins.ltc + coins.money + coins.eth + coins.monero) <= 0)
        {
            SetDefaultValue();
        }
        else
        {
            this.btc = coins.btc;
            this.ltc = coins.ltc;
            this.money = coins.money;
            this.eth = coins.eth;
            this.monero = coins.monero;
        }

    }

    private void SetDefaultValue()
    {
        coins.btc = 0;
        coins.ltc = 0;
        coins.money = 0;
        coins.eth = 0;
        coins.monero = 0;
    }

    public void UpdateShowDataCoins()
    {
        textCoin[0].text = BTC.ToString() + iconBTC;
        textCoin[1].text = LTC.ToString() + iconLTC;
        textCoin[2].text = Money.ToString() + iconMoney;
        textCoin[3].text = ETH.ToString() + iconETH;
        textCoin[4].text = MON.ToString() + iconMON;

    }

    public void OnDisable()
    {
        SaveCoins();
        onCount -= UpdateShowDataCoins;
    }

    private void SaveCoins()
    {
        coins.btc = this.btc;
        coins.ltc = this.ltc;
        coins.money = this.money;
        coins.eth = this.eth;
        coins.monero = this.monero;
        SaveLoad.Save(coins, "Coins.json");
    }


    public void AddMoney(int addmoney)
    {
        Money += addmoney;

    }

}
