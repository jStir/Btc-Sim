﻿using System.IO;
using UnityEngine;


public class SaveLoad
{
    public static void Save<T>(T objToSave, string key)
    {
        string path = Path.Combine(Application.persistentDataPath + "/" + key);
        Debug.Log(path);
        // Directory.CreateDirectory(Application.dataPath +"/saves/");

        if (File.Exists(path))
        {
            File.WriteAllText(path, JsonUtility.ToJson(objToSave));
            Debug.Log("Was save " + objToSave);
        }
        else
        {
            Debug.Log("no path save " + path);
            File.Create(path);
        }
    }

    public static void Load<T>(ref T t, string key)
    {
        string path = Path.Combine(Application.persistentDataPath + "/" + key);
        Debug.Log(Application.persistentDataPath);

        if (File.Exists(path))
        {

            t = (T)JsonUtility.FromJson<T>(File.ReadAllText(path));
            Debug.Log("Load data done!");
        }
        else
        {
            Debug.Log("no path load " + path);
            //Directory.CreateDirectory(Application.persistentDataPath);
            File.Create(path);



        }

    }




}
