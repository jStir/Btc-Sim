﻿public class Item
{
    public int ID { get; set; }
    public string Title { get; set; }
    public int Price { get; set; }
    public int PowerConsumption { get; set; }
    public int[] HashRate { get; set; }
    public bool isRun { get; set; }
    public int Hp { get; set; }

   
    public Item(int id, string title, int price, int powerConsumption, int hashBtc, int hashLtc, int hashEth, int hashMon, int hp, bool isRun)
    {
        this.ID = id;
        this.Title = title;
        this.Price = price;
        this.PowerConsumption = powerConsumption;
        this.HashRate = new int[4];
        this.HashRate[0] = hashBtc;
        this.HashRate[1] = hashLtc;
        this.HashRate[2] = hashEth;
        this.HashRate[3] = hashMon;
        this.isRun = isRun;
        this.Hp = hp;
    }

    public Item(int id, string title, int price, int powerConsumption, int hashBtc, int hashLtc, int hashEth, int hashMon)
    {
        this.ID = id;
        this.Title = title;
        this.Price = price;
        this.PowerConsumption = powerConsumption;
        this.HashRate = new int[4];
        this.HashRate[0] = hashBtc;
        this.HashRate[1] = hashLtc;
        this.HashRate[2] = hashEth;
        this.HashRate[3] = hashMon;
        this.isRun = false;
        this.Hp = 100;
    }
}
